# Welcome to RISE

The open-source architecture **RISE** (A Robotics Integration and Scenario-Management Extensible-Architecture) is a tool for researcher that want to create Human-Robot-Interaction (HRI) scenarios.
RISE offers the possibility to connect initially with the robots Nao, Pepper and is extensible for the use of own robots with custom actions (e.g. the agent Floka)!

The software is developed within the Transregional Collaborative Research Centre "Constructing Explainability" ([TRR 318](https://trr318.uni-paderborn.de/en/)) and is used for internal projects as well as in coorperation with partners.
The main focus of the system is to support the work in interdisciplinary teams and to enable also non-computer scientists to create, discuss and understand behaviors of robots in HRI.

The [documentation](https://rise-projects.pages.ub.uni-bielefeld.de/rise-documentation/contents.html) and also the whole project is under development mainly by (PhD) students and research assistants. 
The system and its documentation exhibit a continuous state of development and change.

<p align="center">
  <img src="https://gitlab.ub.uni-bielefeld.de/rise-projects/rise-documentation/-/raw/development/source/images/gifs/rise_overview_and_control.gif" width="48%" />
    &nbsp; &nbsp; &nbsp; &nbsp;
  <img src="https://gitlab.ub.uni-bielefeld.de/rise-projects/rise-documentation/-/raw/development/source/images/gifs/rise_control_via_ir.gif" width="48%" /> 
</p>

# Group Structure

The GitLab-Group is organized into several subgroups, each responsible for a different aspect of the RISE architecture. Here is an overview of the group's structure:

- Application: This subgroup manages the main application, RISE-Core, configurations, templates for RISE, and a connection tool. The connection tool enables the integration of RISE as a Unity application with a ROS environment.
- Feature-Nodes: This subgroup is dedicated to housing external components and features for present and future projects. A feature-node refers to an information processing node with an integrated bridge to either ROS or RISE directly.
- Robot-Wrapper: Each robot in the group has its own dedicated wrapper, responsible for facilitating communication with its corresponding robot API.
- RISE-Documentation: Maintaining the source code for the RISE documentation.


# Project Responsibility

This project is developed and maintained by the working group..

* [Medical Assistance Systems](https://www.uni-bielefeld.de/fakultaeten/medizin/fakultaet/arbeitsgruppen/assistenzsysteme/) <br>
  Medical School OWL <br>
  Bielefeld University

For any inquiries or questions regarding the project, please reach out to the project contact persons via email:

* André Groß [(contact)](mailto:agross@techfak.uni-bielefeld.de) 
* Christian Schütze [(contact)](mailto:cschuetze@techfak.uni-bielefeld.de) 

# Publication

[RISE in Frontiers in Robotics and AI](https://www.frontiersin.org/articles/10.3389/frobt.2023.1245501/abstract)

```
@article{gross_schuetze_rise,
  title = {RISE: An Open-Source Architecture for Interdisciplinary and Reproducible HRI Research},
  author = {Gro{\ss}, Andr{\'e} and Sch{\"u}tze, Christian and Brandt, Mara and Wrede, Britta and Richter, Birte},
  journal={Frontiers in Robotics and AI},
  volume={10},
  year={2023},
  publisher={Frontiers},
  doi={10.3389/frobt.2023.1236184}
}
```

# License

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]
<br><br>
The source-code of this work is licensed under a [MIT License](https://opensource.org/licenses/MIT).
The documentation of this project is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].


[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg